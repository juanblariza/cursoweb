@component('mail::message')
# Hola, {{$user}}

Tus credenciales de acceso son:


@component('mail::table')
    | Email         | Contraseña    |
    | ------------- |:-------------:|
    | {{$email}}      | {{$password}}      |

@endcomponent

{{--@component('mail::button', ['url' => ''])--}}
{{--Button Text--}}
{{--@endcomponent--}}

Gracias,<br>
{{ config('app.name') }}
@endcomponent
