@extends('layouts.admin')

@section('title')

    Trámites

    <a href="{{route('admin.tramites.create')}}" class="btn btn-info btn-sm">
        <i class="fa fa-plus" aria-hidden="true"></i>
    </a>

@endsection

@section('content')


    <div class="card">

        <div class="card-body">
            <table class="table-bordered table">
                <thead>
                <tr>
                    <th class="text-center">Usuario</th>
                    <th class="text-center">Tipo trámite</th>
                    <th class="text-center">Fecha inicio</th>
                    <th class="text-center">Fecha finalizacion</th>
                    <th class="text-center">Área inicial</th>
                    <th class="text-center">Área actual</th>
                    <th class="text-center">Estado</th>
                    <th class="text-center">Acción</th>
                </tr>
                </thead>
                <tbody>
                @foreach($tramites as $a)
                    <tr class="text-center">
                        <td>
                            {{$a->user->usuario}}
                        </td>
                        <td>
                            {{$a->tipoTramite->tipo_tramite}}
                        </td>
                        <td>
                            {{$a->fecha_ingreso}}
                        </td>
                        <td>
                            {{$a->fecha_finalizacion}}
                        </td>
                        <td>
                            {{$a->areaInicial->area}}
                        </td>
                        <td>
                            {{$a->areaActual->area}}
                        </td>
                        <td>
                            {{$a->estado->estado}}
                        </td>
                        <td>
                            <div class="btn-group">
                                <i class="fa fa-list" aria-hidden="true" data-toggle="dropdown"
                                   aria-expanded="false" style="cursor: pointer; color: #1d68a7">
                                </i>

                                <div class="dropdown-menu" role="menu" x-placement="bottom-start"
                                     style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(67px, 38px, 0px);">
                                    <a class="dropdown-item" href="#">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        Editar
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                        Eliminar
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>

    </div>


@endsection
