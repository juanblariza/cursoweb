@extends('layouts.admin')

@section('title')

    Nuevo Trámite

@endsection

@section('content')


    <div class="card card-info">

        <!-- form start -->
        <form method="POST" action="{{route('admin.tramites.store')}}" class="form-horizontal">
            @csrf

            <div class="card-body">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Usuarios</label>

                    <div class="col-sm-12">
                        <select name="user_id" id="" class="form-control">
                            @foreach($usuarios as $u)
                                <option value="{{$u->id}}">{{$u->usuario}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Tipo de trámite</label>

                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="nombre" value="{{old('nombre')}}" placeholder="..." autofocus  autocomplete="off">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Área inicial</label>

                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="nombre" value="{{old('nombre')}}" placeholder="..." autofocus  autocomplete="off">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Área actual</label>

                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="nombre" value="{{old('nombre')}}" placeholder="..." autofocus  autocomplete="off">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Fecha de Ingreso</label>

                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="nombre" value="{{old('nombre')}}" placeholder="..." autofocus  autocomplete="off">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Fecha de finalación</label>

                    <div class="col-sm-12">
                        <input type="text" class="form-control" name="nombre" value="{{old('nombre')}}" placeholder="..." autofocus  autocomplete="off">
                    </div>
                </div>


            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <input type="submit" class="btn btn-info float-right"  value="Crear"/>
                <a href="{{route('admin.tramites.index')}}" class="btn btn-default">Cancel</a>
            </div>
            <!-- /.card-footer -->
        </form>
    </div>

@endsection
