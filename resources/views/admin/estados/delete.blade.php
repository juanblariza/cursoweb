@extends('layouts.admin')

@section('title')

    Eliminar Estado


@endsection

@section('content')


    <div class="card card-info">

        <div class="card-body">

            <!-- form start -->
            <form method="POST" action="{{route('admin.estados.destroy', $estado->id)}}" class="form-horizontal">
                @csrf
                @method('delete')

                <div class="alert alert-danger text-center" style="overflow: hidden;">

                    ¿ Eliminar el estado <strong>{{$estado->estado}}</strong>?

                </div>

                <a href="{{route('admin.estados.index')}}" class="btn btn-default float-left">Cancel</a>

                <input type="submit" class="btn btn-danger float-right" value="Eliminar"/>

                <!-- /.card-footer -->
            </form>

        </div>
    </div>

@endsection
