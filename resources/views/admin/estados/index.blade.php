@extends('layouts.admin')

@section('title')

    Estados

    <a href="{{route('admin.estados.create')}}" class="btn btn-info btn-sm">
        <i class="fa fa-plus" aria-hidden="true"></i>
    </a>

@endsection

@section('content')



    <div class="card">

        @if(session()->has('success'))
            <div class="alert alert-success">
                {{session()->get('success')}}
            </div>
        @endif

        <div class="card-body">
            <table class="table-bordered table">
                <thead>
                <tr>
                    <td class="text-center">Estado</td>
                    <td class="text-center">Acciones</td>
                </tr>
                </thead>
                <tbody>
                @foreach($estados as $e)
                    <tr class="text-center">
                        <th>
                            {{$e->estado}}
                        </th>

                        <th>
                            <div class="btn-group">
                                <i class="fa fa-list" aria-hidden="true" data-toggle="dropdown"
                                   aria-expanded="false" style="cursor: pointer; color: #1d68a7">
                                </i>

                                <div class="dropdown-menu" role="menu" x-placement="bottom-start"
                                     style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(67px, 38px, 0px);">
                                    <a class="dropdown-item" href="{{route('admin.estados.edit', $e->id)}}">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        Editar
                                    </a>
                                    <a class="dropdown-item" href="{{route('admin.estados.show', $e->id)}}">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                        Eliminar
                                    </a>
                                </div>
                            </div>
                        </th>
                    </tr>
                @endforeach

                </tbody>
            </table>
            {{ $estados->links() }}
        </div>

    </div>


@endsection
