@extends('layouts.admin')

@section('title')
    Usuarios
@endsection

@section('link')
    <li class="breadcrumb-item active">
        <a href="{{route('admin.usuario.index')}}">Usuarios</a>
    </li>
@endsection

@section('content')


    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <h5 class="m-0">Lista de usuarios</h5>
            </div>
            <div class="card-body">

                <table class="table table-bordered ">
                    <thead class="thead-dark">
                    <tr>
                        <th class="text-center">Usuario</th>
                        <th class="text-center">Telefono</th>
                        <th class="text-center">Email</th>
                        <th class="text-center">Rol</th>
                        <th class="text-center">Cargos</th>
                        <th class="text-center">
                            <a class="btn btn-primary btn-sm" href="{{route('admin.usuario.create')}}">Nuevo usuario</a>
                        </th>
                    </tr>
                    </thead>

                    <tbody class="text-center">
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->usuario}}</td>
                            <td>
                                @if($user->telefono)
                                    {{$user->telefono->telefono}}
                                @else
                                    <p class=" badge badge-danger">
                                        No tiene teléfono
                                    </p>
                                @endif

                            </td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->role}}</td>
                            <td>
                                @if($user->cargos)
                                    @foreach($user->cargos as $cargo)
                                        <span class="badge badge-success" style="font-size: 15px;">{{$cargo->cargo}}</span> <br>
                                    @endforeach
                                @endif

                            </td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <i class="fa fa-list" aria-hidden="true" data-toggle="dropdown"
                                       aria-expanded="false" style="cursor: pointer; color: #1d68a7">
                                    </i>

                                    <div class="dropdown-menu" role="menu" x-placement="bottom-start"
                                         style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(67px, 38px, 0px);">
                                        <a class="dropdown-item" href="{{route('admin.cargo_usuario.create', $user->id)}}">
                                            <i class="fa fa-pencil-alt" ></i>
                                            Agregar Cargo
                                        </a>
                                        <a class="dropdown-item" href="{{route('admin.cargo_usuario.show', $user->id)}}">
                                            <i class="fa fa-pencil-alt" aria-hidden="true"></i>
                                            Eliminar Cargo
                                        </a>
                                        <a class="dropdown-item" href="">
                                            <i class="fa fa-pencil-alt" aria-hidden="true"></i>
                                            Editar
                                        </a>
                                        <a class="dropdown-item" href="#">
                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                            Eliminar
                                        </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
