@extends('layouts.admin')

@section('title')

    Nueva Área

@endsection

@section('content')


    <div class="card card-info">

        <!-- form start -->
        <form method="POST" action="{{route('admin.areas.store')}}" class="form-horizontal">
            @csrf

            <div class="card-body">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Área</label>

                    <div class="col-sm-12">
                        <input type="text" class="form-control {{ $errors->has('area') ? ' is-invalid' : '' }}"
                               name="area" value="{{old('area')}}" placeholder="-"
                               autofocus required autocomplete="off">
                    </div>
                    @if ($errors->has('area'))
                        <span class="invalid-feedback d-block" role="alert">
                            <strong>{{ $errors->first('area') }}</strong>
                        </span>
                    @endif
                </div>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <input type="submit" class="btn btn-info float-right" value="Crear"/>
                <a href="{{route('admin.areas.index')}}" class="btn btn-default">Cancel</a>
            </div>
            <!-- /.card-footer -->
        </form>
    </div>

@endsection
