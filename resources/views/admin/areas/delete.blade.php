@extends('layouts.admin')

@section('title')

    Eliminar Área


@endsection

@section('content')


    <div class="card card-info">

        <div class="card-body">

            <!-- form start -->
            <form method="POST" action="{{route('admin.areas.destroy', $area->id)}}" class="form-horizontal">
                @csrf
                @method('delete')

                <div class="alert alert-danger text-center" style="overflow: hidden;">

                    ¿ Eliminar el área <strong>{{$area->area}}</strong>?

                </div>

                <a href="{{route('admin.areas.index')}}" class="btn btn-default float-left">Cancel</a>

                <input type="submit" class="btn btn-danger float-right" value="Eliminar"/>

                <!-- /.card-footer -->
            </form>

        </div>
    </div>

@endsection
