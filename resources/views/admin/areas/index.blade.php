@extends('layouts.admin')

@section('title')

    Areas

    <a href="{{route('admin.areas.create')}}" class="btn btn-info btn-sm">
        <i class="fa fa-plus" aria-hidden="true"></i>
    </a>

@endsection

@section('content')


    <div class="card">



        <div class="card-body ">
            @if(session()->has('success'))
                <div class="alert alert-success mb-3">
                    {{ session()->get('success') }}
                </div>
            @endif
            <table class="table-bordered table mb-2 datatables">
                <thead>
                <tr>
                    <td class="text-center">Área</td>
                    <td class="text-center">Acciones</td>
                </tr>
                </thead>
                <tbody>
                @foreach($areas as $a)
                    <tr class="text-center">
                        <th>
                            {{$a->area}}
                        </th>
                        <th>
                            <div class="btn-group">
                                <i class="fa fa-list" aria-hidden="true" data-toggle="dropdown"
                                   aria-expanded="false" style="cursor: pointer; color: #1d68a7">
                                </i>

                                <div class="dropdown-menu" role="menu" x-placement="bottom-start"
                                     style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(67px, 38px, 0px);">
                                    <a class="dropdown-item" href="{{route('admin.areas.edit', $a->id)}}">
                                        <i class="fa fa-pencil-alt" aria-hidden="true"></i>
                                        Editar
                                    </a>
                                    <a class="dropdown-item" href="{{route('admin.areas.show', $a->id)}}">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                        Eliminar
                                    </a>
                                </div>
                            </div>
                        </th>
                    </tr>
                @endforeach

                </tbody>
            </table>

            {{--<div class="pagination-links">--}}
                {{--{{$areas->links()}}--}}
            {{--</div>--}}
        </div>


    </div>


@endsection
