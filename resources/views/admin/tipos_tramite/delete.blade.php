@extends('layouts.admin')

@section('title')

    Eliminar Tipo de Trámite


@endsection

@section('content')


    <div class="card card-info">

        <div class="card-body">

            <!-- form start -->
            <form method="POST" action="{{route('admin.tipos_tramite.destroy', $tipo_tramite->id)}}" class="form-horizontal">
                @csrf
                @method('delete')

                <div class="alert alert-danger text-center" style="overflow: hidden;">

                    ¿ Eliminar el tipo de tramite <strong>{{$tipo_tramite->tipo_tramite}}</strong>?

                </div>

                <a href="{{route('admin.tipos_tramite.index')}}" class="btn btn-default float-left">Cancel</a>

                <input type="submit" class="btn btn-danger float-right" value="Eliminar"/>

                <!-- /.card-footer -->
            </form>

        </div>
    </div>

@endsection
