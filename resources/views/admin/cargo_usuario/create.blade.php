@extends('layouts.admin')


@section('title')

    Nuevo Cargo de {{$usuario->usuario}}

@endsection

@section('content')


    <div class="card card-info">

        <!-- form start -->
        <form method="POST" action="{{route('admin.cargo_usuario.store', $usuario->id)}}" class="form-horizontal">
            @csrf


            <div class="card-body">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Cargos</label>

                    <div class="col-sm-12">
                        <select name="cargo_id[]" id="cargo_id" class="form-control multiple" multiple="multiple">
                            @foreach($cargos as $c)
                                <option value="{{$c->id}}">{{$c->cargo}}</option>
                            @endforeach

                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Fecha</label>

                    <div class="col-sm-12">
                        <input name="cargo_id[fecha]" id="cargo_id" class="form-control" />

                    </div>
                </div>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <input type="submit" class="btn btn-info float-right" value="Crear"/>
                <a href="{{route('admin.cargos.index')}}" class="btn btn-default">Cancel</a>
            </div>
            <!-- /.card-footer -->
        </form>
    </div>

@endsection
