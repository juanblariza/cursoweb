<?php

namespace App\Http\Controllers;

use App\Actividad;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index(Request $request)
    {
        return view('home');
    }
}
