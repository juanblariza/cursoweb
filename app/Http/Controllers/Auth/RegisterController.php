<?php

namespace App\Http\Controllers\Auth;

use App\Mail\UsuarioRegistrado;
use App\Telefono;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = '/home';

    public function index()
    {
        $users = User::get();

        return view('admin.users.index', compact('users'));
    }

    public function createForm()
    {
        return view('admin.users.create');
    }

    public function store(Request $r)
    {

        $t = DB::transaction(function () use ($r) {

            $user = User::create([
                'name' => $r->input('name'),
                'lastname' => $r->input('lastname'),
                'fecha_nacimiento' => $r->input('fecha_naciemiento'),
                'email' => $r->input('email'),
                'area' => $r->input('area'),
                'password' => bcrypt($r->input('password'))
            ]);

            if (!is_null($r->input('telefono'))) {
                Telefono::create([
                    'telefono' => $r->input('telefono'),
                    'user_id' => $user->id
                ]);
            }

            Mail::to($user->email)->queue(new UsuarioRegistrado($user->email, $r->input('password'), $user->usuario));


            return redirect(route('admin.usuario.index'));

        });


        return $t;

    }


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'area' => 'required|string|max:255',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    /*//    protected function create(array $data)
    //    {
    //        return User::create([
    //            'name' => $data['name'],
    //            'lastname' => $data['lastname'],
    //            'email' => $data['email'],
    //            'area' => $data['area'],
    //            'password' => Hash::make($data['password']),
    //        ]);
    //    }*/


}
