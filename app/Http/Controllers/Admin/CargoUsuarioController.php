<?php

namespace App\Http\Controllers\Admin;

use App\Cargo;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CargoUsuarioController extends Controller
{

    public function create(User $usuario)
    {
        $cargos = Cargo::get();

        return view('admin.cargo_usuario.create', compact('cargos', 'usuario'));
    }

    public function show(User $usuario)
    {

        return view('admin.cargo_usuario.delete', compact('usuario'));
    }

    public function store(Request $request, User $usuario)
    {

        dd($request->all());

        $usuario->cargos()->attach($request->input('cargo_id'));

//        foreach ($request->input('cargo_id') as $id) {
//
//            $usuario->cargos()->attach($id,
//                ['fecha_inicio' => $request->input('fecha')]
//            );
//
//        }


        return redirect(route('admin.usuario.index'));
    }

    public function update(Request $request, $id)
    {
        Cargo::where('id', $id)->update([
            'cargo' => $request->input('cargo')
        ]);

        return redirect(route('admin.cargos.index'));
    }


}
