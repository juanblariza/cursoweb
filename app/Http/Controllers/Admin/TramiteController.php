<?php

namespace App\Http\Controllers\Admin;

use App\Area;
use App\TipoTramite;
use App\Tramite;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TramiteController extends Controller
{
    public function index()
    {
        $tramites = Tramite::get();

        return view('admin.tramites.index', compact('tramites'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $usuarios = User::get();

        $tipoTramite = TipoTramite::get();

        $areas = Area::get();

        return view('admin.tramites.create', compact('usuarios'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Tramite::create([
            'fecha_ingreso' => '2000-01-01',
            'fecha_finalizacion' => '2000-01-01',
            'tipo_tramite_id' => 1,
            'area_inicial_id' => 1,
            'area_actual_id' => 1,
            'estado_id' => 1,
            'user_id' => $request->input('user_id')
        ]);


        return redirect(route('admin.tramites.index'));

    }
}
