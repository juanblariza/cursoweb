<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tramite extends Model
{

    protected $table = "tramites";

    protected $fillable = [
        'fecha_ingreso', 'fecha_finalizacion', 'tipo_tramite_id', 'area_inicial_id',
        'area_actual_id', 'estado_id', 'user_id'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function tipoTramite(){
        return $this->belongsTo(TipoTramite::class, 'tipo_tramite_id');
    }

    public function areaInicial(){
        return $this->belongsTo(Area::class, 'area_inicial_id');
    }
    public function areaActual(){
        return $this->belongsTo(Area::class, 'area_actual_id');
    }

    public function estado(){
        return $this->belongsTo(Estado::class);
    }

}
