<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UsuarioRegistrado extends Mailable
{
    use Queueable, SerializesModels;

    public $email;
    public $password;
    public $user;

    public function __construct($email, $password, $user)
    {
        $this->email = $email;
        $this->password = $password;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.usuario.registro');
    }
}
