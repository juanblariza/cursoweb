<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'lastname', 'email', 'area', 'password', 'fecha_nacimiento'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = 'usuario';

    public function setFechaNacimientoAttribute($value)
    {
        $this->attributes['fecha_nacimiento'] = date('Y-m-d', strtotime($value));
    }

    public function setNameAttribute($value){
        $this->attributes['name'] = ucwords(strtolower($value));
    }

    public function getFechaNacimientoAttribute(){
        return date('Y-m-d', strtotime($this->fecha_nacimiento));
    }

    public function getUsuarioAttribute(){
        return "$this->name $this->lastname";
    }

    /*
     * RELACIONES
     */

    public function telefono()
    {
        return $this->hasOne(Telefono::class);
    }


    public function cargos(){
        return $this->belongsToMany(Cargo::class,'cargo_user')
            ->withPivot(['fecha_inicio']);
    }
}
