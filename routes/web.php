<?php

Route::group(['middleware' => 'guest'], function () {

    // Authentication Routes...
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');

    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

});


Route::group(['middleware' => 'auth'], function () {

    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index')->name('home');

    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    Route::group(['middleware' => 'admin', 'as' => 'admin.', 'prefix' => 'admin'], function () {

        // Users Routes...
        Route::get('usuario', 'Auth\RegisterController@index')->name('usuario.index');

        Route::post('usuario', 'Auth\RegisterController@store')->name('usuario.store');

        Route::get('usuario/create', 'Auth\RegisterController@createForm')->name('usuario.create');

    });
});

