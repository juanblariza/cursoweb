<?php


Route::get('home', 'HomeController@index')->name('home.index');

Route::resource('estados', 'EstadoController');

Route::resource('areas', 'AreaController');

Route::resource('tipos_tramite', 'TipoTramiteController');

Route::resource('tramites', 'TramiteController');

Route::resource('cargos', 'CargoController');

Route::get('register', 'RegisterControlleAuth\r@showRegistrationForm')->name('register');

Route::get('cargo/usuario/create/{usuario}', 'CargoUsuarioController@create')->name('cargo_usuario.create');
Route::get('cargo/usuario/show/{usuario}', 'CargoUsuarioController@show')->name('cargo_usuario.show');
Route::post('cargo/usuario/store/{usuario}', 'CargoUsuarioController@store')->name('cargo_usuario.store');