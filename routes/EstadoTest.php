<?php

namespace Tests\Feature;

use App\Estado;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EstadoTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function un_estado_es_creado()
    {
        $user = factory(User::class)->create([
            'role' => 'admin'
        ]);

        $this->actingAs($user);

        $this->post(route('admin.estados.store'), [
            'estado' => 'Prueba'
        ])->assertRedirect(route('admin.estados.index'))
            ->assertStatus(302);

        $this->assertDatabaseHas('estados',[
            'estado' => 'Prueba'
        ]);
    }

    /** @test */
    public function un_estado_es_actualizado()
    {

        $user = factory(User::class)->create([
            'role' => 'admin'
        ]);

        $this->actingAs($user);

        $estado = factory(Estado::class)->create([
            'estado' => 'nuevo'
        ]);

        $this->put(route('admin.estados.update', $estado->id), [
            'estado' => 'usado'
        ])->assertRedirect(route('admin.estados.index'))
            ->assertStatus(302);

        $this->assertDatabaseHas('estados',[
            'estado' => 'usado'
        ]);

        $this->assertDatabaseMissing('estados',[
            'estado' => $estado->estado
        ]);
    }
}
