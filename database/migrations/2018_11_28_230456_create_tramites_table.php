<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTramitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tramites', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('fecha_ingreso');
            $table->dateTime('fecha_finalizacion')->nullable();

            $table->unsignedInteger('user_id');
            $table->unsignedInteger('tipo_tramite_id');
            $table->unsignedInteger('area_inicial_id');
            $table->unsignedInteger('area_actual_id');
            $table->unsignedInteger('estado_id');

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('tipo_tramite_id')->references('id')->on('tipos_tramites');
            $table->foreign('area_inicial_id')->references('id')->on('areas');
            $table->foreign('area_actual_id')->references('id')->on('areas');
            $table->foreign('estado_id')->references('id')->on('estados');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tramites');
    }
}
