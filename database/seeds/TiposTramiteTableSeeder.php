<?php

use Illuminate\Database\Seeder;

class TiposTramiteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipos_tramites')->insert([
            'tipo_tramite' => 'Registro' ,
        ]);

        DB::table('tipos_tramites')->insert([
            'tipo_tramite' => 'Desacrgo' ,
        ]);

        DB::table('tipos_tramites')->insert([
            'tipo_tramite' => 'Solicitud de información' ,
        ]);


    }
}
