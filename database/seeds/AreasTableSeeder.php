<?php

use Illuminate\Database\Seeder;

class AreasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('areas')->insert([
            'area' => 'Adminitración' ,
        ]);

        DB::table('areas')->insert([
            'area' => 'Técnica' ,
        ]);

        DB::table('areas')->insert([
            'area' => 'Sistemas' ,
        ]);

        DB::table('areas')->insert([
            'area' => 'RRHH' ,
        ]);

    }
}
