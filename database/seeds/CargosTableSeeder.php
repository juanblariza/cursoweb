<?php

use Illuminate\Database\Seeder;

class CargosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cargos')->insert([
            'cargo' => 'Operario',
        ]);

        DB::table('cargos')->insert([
            'cargo' => 'Gerente',
        ]);

        DB::table('cargos')->insert([
            'cargo' => 'CEO',
        ]);

        DB::table('cargos')->insert([
            'cargo' => 'CTO',
        ]);

        DB::table('cargos')->insert([
            'cargo' => 'Desarrollador',
        ]);


        DB::table('cargo_user')->insert([
            'user_id' => 3,
            'cargo_id' => 1,
             'fecha_inicio' => '2018-08-05'
        ]);

        DB::table('cargo_user')->insert([
            'user_id' => 3,
            'cargo_id' => 2,
            'fecha_inicio' => '2018-08-05'
        ]);
//
        DB::table('cargo_user')->insert([
            'user_id' => 3,
            'cargo_id' => 3,
            'fecha_inicio' => '2018-08-05'
        ]);

        DB::table('cargo_user')->insert([
            'user_id' => 4,
            'cargo_id' => 2,
            'fecha_inicio' => '2018-08-05'
        ]);
//
        DB::table('cargo_user')->insert([
            'user_id' => 5,
            'cargo_id' => 3,
            'fecha_inicio' => '2018-08-05'
        ]);

        DB::table('cargo_user')->insert([
            'user_id' => 5,
            'cargo_id' => 4,
            'fecha_inicio' => '2018-08-05'
        ]);
    }
}
