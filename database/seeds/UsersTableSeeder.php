<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Guillermo',
            'lastname' => 'Colotti',
            'email' => 'admin@admin.com',
            'role' => 'admin',
            'password' => bcrypt('admin')
        ]);

        DB::table('users')->insert([
            'name' => 'Pepe',
            'lastname' => 'Martinez',
            'email' => 'user@user.comx',
            'role' => 'user',
            'password' => bcrypt('user')
        ]);

        DB::table('users')->insert([
            'name' => 'Martin',
            'lastname' => 'Martinez',
            'email' => 'user@user.comm',
            'role' => 'user',
            'password' => bcrypt('user')
        ]);

        DB::table('users')->insert([
            'name' => 'Maria',
            'lastname' => 'Lopez',
            'email' => 'user@user.coma',
            'role' => 'user',
            'password' => bcrypt('user')
        ]);

        DB::table('users')->insert([
            'name' => 'Pepe',
            'lastname' => 'Argento',
            'email' => 'user@user.comaxc',
            'role' => 'user',
            'password' => bcrypt('user')
        ]);


//        factory(\App\User::class, 10)->create();
    }
}
