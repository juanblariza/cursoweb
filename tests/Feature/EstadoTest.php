<?php

namespace Tests\Feature;

use App\Estado;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EstadoTest extends TestCase
{

    use DatabaseTransactions;

    /** @test */
    public function estado_creado_correctamente()
    {
        $this->userLogin();

        $this->post(route('admin.estados.store'),[
            'estado' => 'En Proceso'
        ])->assertRedirect(route('admin.estados.index'));

        $this->assertDatabaseHas('estados',[
           'estado' => 'En Proceso'
        ]);
    }

    /** @test */
    public function estado_actualizado_correctamente()
    {
        $this->userLogin();

        $estado = factory(Estado::class)->create([
            'estado' => 'pendiente'
        ]);

        $this->put(route('admin.estados.update', $estado->id),[
            'estado' => 'finalizado'
        ])->assertRedirect(route('admin.estados.index'));

        $this->assertDatabaseHas('estados',[
           'estado' => 'finalizado'
        ]);

        $this->assertDatabaseMissing('estados',[
           'estado' =>  $estado->estado
        ]);

    }

    private function userLogin(){
        $user = factory(User::class)->create([
            'role' => 'admin'
        ]);

        $this->actingAs($user);
    }
}
