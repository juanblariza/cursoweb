<?php

namespace Tests\Browser;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{

    public function test_usuario_puede_iniciar_sesion()
    {

        $this->browse(function (Browser $browser) {

            $user = factory(User::class)->create();

            $browser->visit('/login')
                ->type('email',$user->email)
                ->type('password', 'user')
                ->press('Acceder')
                ->assertPathIs('/home');
        });
    }
}
